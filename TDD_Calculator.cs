﻿using System;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace fr_stringcalculator
{

    [TestFixture]

    public class Class1

    {
        [Test]

        public void Num_0_Calculator_Recibe_blanco()

        {

            Assert.IsTrue ( Calculator.getSum("")==0);

        }
        [Test]

        public void Num_1_Calculator_Recibe_texto()

        {

            Assert.IsTrue(Calculator.getSum("asdfas") == 0);

        }

        [Test]

        public void Num_2_Calculator_Recibe_1()

        {

            Assert.IsTrue(Calculator.getSum("1") == 1);

        }

        [Test]
        public void Num_3_Calculator_Recibe_1_Y_2_Y_3()

        {

            Assert.IsTrue(Calculator.getSum("1\\n2,3") == 6);

        }

        [Test]
        public void Num_4_Calculator_Recibe_blanco_Y_2()

        {

            Assert.IsTrue(Calculator.getSum(",2") == 2);

        }

        [Test]
        public void Num_5_Calculator_Recibe_blanco_Y_blanco()

        {

            Assert.IsTrue(Calculator.getSum(",") == 0);

        }

        [Test]
        public void Num_6_Calculator_Recibe_100_unos()

        {
            string cadena_de_unos = "";
            for (int i = 1; i <= 100; i++)
            {
                cadena_de_unos += "1,";
            }

            cadena_de_unos = cadena_de_unos.Substring(0, cadena_de_unos.Length -1);
            Assert.IsTrue(Calculator.getSum(cadena_de_unos) ==100);

        }

        [Test]
        public void Num_7_Calculator_Recibe_1_Y_2_y_delimitador()

        {

            Assert.IsTrue(Calculator.getSum("//;\\n1;2") == 3);

        }

        [Test]
        public void Num_6_Calculator_Recibe_100_unos_y_delimitador()

        {
            string cadena_de_unos = "";
            for (int i = 1; i <= 100; i++)
            {
                cadena_de_unos += "1j";
            }

            cadena_de_unos = cadena_de_unos.Substring(0, cadena_de_unos.Length - 1);
            Assert.IsTrue(Calculator.getSum("//j\\n"+cadena_de_unos) == 100);

        }

        [Test]
        public void Num_9_Calculator_Excepcion_un_negativo()

        {
             
            Exception expectedExcetpion = null;
            try
            {
                Calculator.getSum("//;\\n-1;2");
            }
            catch (Exception ex)
            {
                                expectedExcetpion = ex;
                
            }
            Console.WriteLine(expectedExcetpion);
            //Assert.IsNotNull(expectedExcetpion);
        }

        [Test]
        public void Num_10_Calculator_Excepcion_Muchos_negativos()

        {

            Exception expectedExcetpion = null;
            try
            {
                 Calculator.getSum("//;\\n-1;-2;-3;-4");
            }
            catch (Exception ex)
            {
                expectedExcetpion = ex;

            }
            Console.WriteLine(expectedExcetpion);
            //Assert.IsNotNull(expectedExcetpion);
        }

        [Test]
        public void Num_11_Calculator_Error_detectado()

        {

            Assert.IsTrue(Calculator.getSum("//*\\n1*2*2*3") == 8);

        }

        [Test]
        public void Num_12_Calculator_Error_detectado_salto_de_linea()

        {

            Assert.IsTrue(Calculator.getSum("1\\n,2,3") == 6);

        }
    }

}

