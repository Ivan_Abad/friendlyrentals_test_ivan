﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    class Calculator
    {
        public static int getSum(string param1)
        {
            int resultado = 0;

            List<string> ListaValores = new List<string>();
            ListaValores = Obtiene_Lista_Valores(param1);
            limpia_Lista(ref ListaValores);

            resultado = ListaValores.Sum(valores => convierte_a_int(valores));
            //foreach (string num in ListaValores)
            //{
                
            //        resultado += convierte_a_int(num);
               
            //}

            return resultado;
        }
        private static void limpia_Lista(ref List<string> listaValores)
        {
            List<string> ListaValoresNegativos = new List<string>();
            List<string> ListaValoresNumericos = new List<string>();
            ListaValoresNumericos = listaValores.Where(num => Es_num(num) == true).ToList();
            ListaValoresNegativos = ListaValoresNumericos.Where(num =>  convierte_a_int(num) < 0).ToList();
            listaValores = ListaValoresNumericos.Where(num => convierte_a_int(num) > 0).ToList(); ;
            //foreach (string num in listaValores)
            //{
            //    if (Es_num(num))
            //    {
            //        if (convierte_a_int(num)>0)
            //        {                        
            //            ListaValoresNumericos.Add(num);                        
            //        }
            //        else{
            //            ListaValoresNegativos.Add(num);
            //        }
            //    }
            //}
            MuestraExcepcionNegativos(ListaValoresNegativos);
            
            }

        private static void MuestraExcepcionNegativos(List<string> listaValoresNegativos)
        {
            if (listaValoresNegativos.Count > 0)
            {
                Console.Write("negatives not allowed: " + string.Join(" / ", listaValoresNegativos) + "\n");
                              
            }
        }

        private static List<string> Obtiene_Lista_Valores(string param1)
        {
            List<string> ListaValores;
            char[] delimiterChars = { ','};

            Añade_nuevo_Delimitador(ref param1, ref delimiterChars);

            ListaValores = param1.Replace("\\n",",").Split(delimiterChars).ToList();
            return ListaValores;
        }

        private static void Añade_nuevo_Delimitador(ref string param1, ref char[] delimiterChars)
        {
            string NuevoDelimitador = buscadelimitador_en_la_candena(ref param1);

            if (NuevoDelimitador != "")
            {
                Array.Resize(ref delimiterChars, delimiterChars.Length + 1);
                delimiterChars[delimiterChars.Length - 1] = Convert.ToChar(NuevoDelimitador);
            }
        }

        private static string buscadelimitador_en_la_candena(ref string param1)
        {
            string delimintador = "";
            if (param1.IndexOf("//") > -1)
            {
                delimintador = param1.Substring(0, param1.IndexOf("\\n")).Replace("//", "");
                param1 = Retira_delimitador_de_la_lista(param1);
            }
            return delimintador;
        }

        private static string Retira_delimitador_de_la_lista(string param1)
        {
            param1 = param1.Substring(param1.IndexOf("\\n") + 2);
            return param1;
        }

        private static bool Es_num(string param1)
        {
            
                int n;
                bool isNumeric = int.TryParse(param1, out n);
                return isNumeric;
           
        }

        private static int convierte_a_int(string valor)
        {
           
            int valorconvertido = 0;

            valorconvertido= int.Parse(valor);

                            return valorconvertido;
           
        }

       
    }
}
